
public class Foo {
	public static void main(String[] args) {
		Foo.greet();
	}

	public static void greet() {
		System.out.println("Allo!");
	}

	public static void doSomething() {
		// ...
	}
}